package Montero.Lizbeth.ui.sample;

import Montero.Lizbeth.tl.ControllerUsuario;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class ControllerComienzo {

    @FXML
    private TextField nombre;

    @FXML
    private TextField userName;

    @FXML
    private TextField password;

    @FXML
    private TextField tipo;
    @FXML
    private String tUsuario;
    @FXML
    private DatePicker fecha;


    @FXML
    private Button ingresar;
    static ControllerUsuario gestorUsuario;

    static {
        try {
            gestorUsuario = new ControllerUsuario();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void iniciar(ActionEvent event) throws Exception {


        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        String nombreUsuario = nombre.getText();
        String correoUsuario = userName.getText();
        String contraseniaUsuario = password.getText();
        //String tipoUsuario = tipo.getText();
        String busqueda = gestorUsuario.buscarUsuario(correoUsuario);
        String usuario = gestorUsuario.buscarUsuario(correoUsuario);
        //la validación del usuario
        if (!usuario.equals("")) {
            String[] usu = busqueda.split(",");

            if (usu[1].equalsIgnoreCase(correoUsuario) & usu[2].equals(contraseniaUsuario)) {
                nombreUsuario = usu[0];
                //gestorUsuario.listarUsuario();

                new ToScene().toScene("prueba.fxml", event);
                // new ToScene().toScene("mostrarinfo.fxml", event);
            } else {
                alert.setHeaderText(null);
                alert.setTitle("Info");
                alert.setContentText("Los datos que ingresó no coinciden ");
                alert.showAndWait();
            }

        } else {
            alert.setHeaderText(null);
            alert.setTitle("info");
            alert.setContentText("El usuario no existe");
            alert.showAndWait();

        }
        nombre.clear();
        userName.clear();
        password.clear();

    }

    public void agregar(ActionEvent event) throws Exception {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        String nombreUsuario = nombre.getText();
        String correoUsuario = userName.getText();
        String contraseniaUsuario = password.getText();
        String busqueda = gestorUsuario.buscarUsuario(correoUsuario);
        String usuario = gestorUsuario.buscarUsuario(correoUsuario);
        String tipoUsuario = tipo.getText();
        LocalDate nacimiento = LocalDate.parse(fecha.getValue().toString());

        //la validación del usuario

        if (calcularEdad(18) <= 18) {
            alert.setHeaderText(null);
            alert.setTitle("Información");
            alert.setContentText("Debe ser mayor de edad para poder registrarse");
            alert.showAndWait();
            fecha.requestFocus();
            return;
        }
        if (!usuario.equals("")) {

            String[] usu = busqueda.split(",");
            if (usu[1].equalsIgnoreCase(correoUsuario) & usu[2].equals(contraseniaUsuario)) {
                nombreUsuario = usu[0];
                alert.setTitle("Info");
                alert.setContentText("El usuario ya existe en el sistema ");
                alert.showAndWait();
            } else {
                alert.setHeaderText(null);
                alert.setTitle("Info");
                alert.setContentText("Error al registrar usuario");
                alert.showAndWait();
            }

        } else {
            gestorUsuario.registrarUsuario(nombreUsuario, correoUsuario, contraseniaUsuario, tipoUsuario);
            alert.setHeaderText(null);
            alert.setTitle("info");
            alert.setContentText("Usuario registrado correctamente");
            alert.showAndWait();

            new ToScene().toScene("prueba.fxml", event);
        }
        nombre.clear();
        userName.clear();
        password.clear();
        tipo.clear();
    }



    public void inicioVendedor(ActionEvent event) throws Exception {
        new ToScene().toScene("prueba.fxml", event);
    }

    public int calcularEdad(int i) throws ParseException {
        int edad = 0;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        LocalDate today = LocalDate.now();

        String formattedDate;
        formattedDate = today.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        Date fechaInicial = dateFormat.parse(formattedDate);
        Date fechaFinal = dateFormat.parse(fecha.getValue().toString());

        long in = fechaInicial.getTime();
        long fin = fechaFinal.getTime();
        Long diff = (in - fin) / 1000;
        return diff.intValue() / (60 * 60 * 24 * 365);
    }

}
