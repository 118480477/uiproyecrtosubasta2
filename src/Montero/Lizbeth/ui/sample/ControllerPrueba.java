package Montero.Lizbeth.ui.sample;

import Montero.Lizbeth.tl.ControllerUsuario;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;


import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class ControllerPrueba {

    @FXML
    private Button perfil;

    @FXML
    private Button cerrar;

    @FXML
    private Button colec;

    @FXML
    private Button admin;

    @FXML
    void ADMIISTRADOR(ActionEvent event) throws Exception {
        new ToScene().toScene("ADD.fxml", event);

    }

    @FXML
    void IrColeccionis(ActionEvent event) throws Exception {
        new ToScene().toScene("Colecc.fxml", event);

    }

    @FXML
    void cerrar(ActionEvent event) throws Exception {
        new ToScene().toScene("Comienzo.fxml", event);

    }

    @FXML
    void perfil(ActionEvent event) {

    }

    static ControllerUsuario gestorUsuario;
    static {
        try {
            gestorUsuario = new ControllerUsuario();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void perfil(Event event) throws Exception {
        gestorUsuario.listarUsuario();

    }

    public void VENDEDOR(ActionEvent event) throws Exception {
        new ToScene().toScene("Vendedor.fxml", event);
    }

    public void IrSubasta(ActionEvent event) throws Exception {
        new ToScene().toScene("subasta.fxml", event);
    }
}
