package Montero.Lizbeth.ui.sample;

import Montero.Lizbeth.tl.ControllerCategoria;
import Montero.Lizbeth.tl.ControllerItem;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.time.LocalDate;
import java.util.List;

public class controllerIO {

    static ControllerItem gestorIT;

    static {
        try {
            gestorIT = new ControllerItem();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @FXML
    private ListView<String> lvListView;

    @FXML
    private TextField codOb;

    @FXML
    private TextField NOMBRE;

    @FXML
    private TextField DESCRI;

    @FXML
    private TextField ESTADO;

    @FXML
    private TextField IMAGEN;

    @FXML
    private TextField ANTIGUEDAD;

    @FXML
    private DatePicker FECHACOMPA;

    @FXML
    private TextField CODCAT;

    @FXML
    private RadioButton REGISTRAROB;
    @FXML
    private Button ObCOLECC;

    @FXML
    private TextField userColecc;

    @FXML
    private TextField codItem;

    @FXML
    private TextField nombreC;
    @FXML
    private ListView<String> listaCat;

    @FXML
    private TextField codC;
    static ControllerCategoria gestorCategoria;

    static {
        try {
            gestorCategoria = new ControllerCategoria();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void REGISTRAROO(ActionEvent event) throws Exception {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        String CD = codOb.getText();
        String IMG = IMAGEN.getText();
        String NOM = NOMBRE.getText();
        String ESTA = ESTADO.getText();
        String DESCR = DESCRI.getText();
        String ANT = ANTIGUEDAD.getText();
        String CCAT = CODCAT.getText();
        LocalDate FC = LocalDate.parse(FECHACOMPA.getValue().toString());
        String busqueda = gestorIT.buscarItem(CD);
        String OB = gestorIT.buscarItem(CD);
        alert.setContentText( gestorIT.registrarItem(CD, NOM, DESCR, ESTA, IMG, FC, ANT, CCAT));
            alert.setHeaderText(null);
            alert.setTitle("info");
            alert.setContentText("item registrado correctamente");
            alert.showAndWait();
        }

    public void BUSCAR(ActionEvent event) throws Exception {
        String CD = codOb.getText();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setTitle("Información");
        String A =CD;
        alert.setContentText(gestorIT.buscarItem(CD));
        alert.showAndWait();
        codOb.clear();
    }

    public void Borrar(ActionEvent event) throws Exception {
        String CD = codOb.getText();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setTitle("Información");
        alert.setContentText(gestorIT.eliminarItem(CD));
        alert.showAndWait();
    }

    public void LISTAR(ActionEvent event) throws Exception {
        List<String> lista = gestorIT.listarObjeto();
        ObservableList<String> observableList = FXCollections.observableList(lista);
        lvListView.setItems(observableList);
    }

    public void volver(ActionEvent event) throws Exception {
        new ToScene().toScene("Colecc.fxml", event);
    }

    public void ITEMCOLECCIONISTA(ActionEvent event) throws Exception {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        String usu = userColecc.getText();
        String itemco = codItem.getText();
        alert.setTitle("Información");
        alert.setContentText(gestorIT.asociarObjetosColeccionista(usu,itemco));
        alert.setContentText("Objeto asociado correctamente");
        alert.showAndWait();
    }

    public void registro(ActionEvent event) {
        String nombreCat= nombreC.getText();
        String codigoC= codC.getText();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Información");
        alert.setContentText(gestorCategoria.registrarCategoria(codigoC,nombreCat));
        alert.showAndWait();

    }

    public void listar(ActionEvent event) throws Exception {
        List<String> lista = gestorCategoria.listarCategoria();
        ObservableList<String> observableList = FXCollections.observableList(lista);
        listaCat.setItems(observableList);

    }

    public void borrar(ActionEvent event) throws Exception {
        String codigoC = codC.getText();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Info");
        alert.setHeaderText(null);
        alert.setTitle(gestorCategoria.eliminarCategoria(codigoC));
        alert.showAndWait();

    }
}






