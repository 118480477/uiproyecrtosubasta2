package Montero.Lizbeth.ui.sample;

import Montero.Lizbeth.tl.ControllerOferta;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;

import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.time.LocalDate;
import java.util.List;


public class contOferta {
    static ControllerOferta gestorOferta;

    static {
        try {
            gestorOferta = new ControllerOferta();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @FXML
    private TextField codoferta;

    @FXML
    private TextField precio;
    
    @FXML
    private TextField user;

    @FXML
    private TextField codiSubasta;

    @FXML
    private DatePicker fechas;

    @FXML
    private Button listO;

    @FXML
    private Button bus;

    @FXML
    private Button eliO;
    @FXML
    private ListView<String> L;
    @FXML
    void BUSCAR(ActionEvent event) {

    }

    @FXML
    void ELIMINAR(ActionEvent event) {

    }

    @FXML
    void LISTAR(ActionEvent event) {

    }

    public void guardar(javafx.event.ActionEvent event) throws Exception {

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        double Prec = Double.parseDouble(precio.getText());
        String codigo= codoferta.getText();
        String cod=codiSubasta.getText();
        String useName= user.getText();
        LocalDate FEcha = LocalDate.parse(fechas.getValue().toString());

        String busqueda = gestorOferta.buscarOferta(codigo);
        String subasta = gestorOferta.buscarOferta(codigo);
        String estado = "";
        //la validacion del check de la acreditacion y la activacion
        if (codigo.equals(gestorOferta.buscarOferta(codigo))) {
            alert.setTitle("info");
            alert.setContentText("esta oferta ya existe");
            alert.showAndWait();
        } else {
            alert.setHeaderText(null);
            alert.setTitle("info");
            alert.setContentText(gestorOferta.registrarOferta(codigo,Prec,FEcha,useName,cod));
            alert.showAndWait();
        }
    }

    public void BUSCAR(javafx.event.ActionEvent event) throws Exception {
        String codigo= codoferta.getText();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setTitle("Información");
        alert.setContentText(gestorOferta.buscarOferta(codigo));
        alert.showAndWait();
        codoferta.clear();


    }

    public void LISTAR(javafx.event.ActionEvent event) throws Exception {
        List<String> lista = gestorOferta.listarOferta();
        ObservableList<String> observableList = FXCollections.observableList(lista);
        L.setItems(observableList);




    }
}
