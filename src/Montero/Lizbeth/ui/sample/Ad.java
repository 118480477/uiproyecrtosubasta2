package Montero.Lizbeth.ui.sample;

import Montero.Lizbeth.tl.ControllerAdministrador;
import Montero.Lizbeth.tl.ControllerColeccionista;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import java.awt.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

public class Ad {
    @FXML
    private TextField userName;

    @FXML
    private TextField nombre;

    @FXML
    private TextField pasword;

    @FXML
    private TextField tipo;

    @FXML
    private TextField edad;

    @FXML
    private TextField estado;

    @FXML
    private TextField correo;

    @FXML
    private TextField estado1;
    @FXML
    private TextField userColecc;

    @FXML
    private TextField identificacion;

    @FXML
    private DatePicker fecha;

    @FXML
    private Button listar;

    @FXML
    private ListView<String> lvc;

    @FXML
    private ListView<String> lv;
    @FXML
    private Button mod;

    @FXML
    private Button elimi;

    @FXML
    private Button ingresar;

    @FXML
    private Button cerrar;


    @FXML
    private TextField AV;

    static ControllerAdministrador gestorAd;

    static {
        try {
            gestorAd = new ControllerAdministrador();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    static ControllerColeccionista gestorColec;

    static {
        try {
            gestorColec = new ControllerColeccionista();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void LISTAR(ActionEvent event) throws Exception {

        List<String> lista = gestorAd.listarAdministrador();
        ObservableList<String> observableList = FXCollections.observableList(lista);
        lv.setItems(observableList);
    }

    public void REGISTRAR(ActionEvent event) throws Exception {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        String user = userName.getText();
        String nom = nombre.getText();
        int edadCole = calcularEdad(Integer.parseInt(edad.getText()));
        String paswod = pasword.getText();
        String ident = identificacion.getText();
        String est = estado.getText();
        String tip= "Administrador";
        String corr= correo.getText();
        String Avat= AV.getText();
        LocalDate nacimiento = LocalDate.parse(fecha.getValue().toString());
        String busqueda = gestorAd.buscarAdministrador(user);
        String A =gestorAd.buscarAdministrador(user);

       if (!A.equals("no existe")) {
           String[] v = busqueda.split(",");
            alert.setTitle("Información");
            alert.setContentText(gestorAd.registrarAdmi(ident,tip,nom,Avat,nacimiento,edadCole,user,corr,paswod,est));
            alert.showAndWait();
        } else {
            alert.setHeaderText(null);
            alert.setTitle("Info");
            alert.setContentText("Los datos que ingresó no coinciden ");
            alert.showAndWait();
        }
    }
    public int calcularEdad(int i) throws ParseException {
        int edad = 0;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        LocalDate today = LocalDate.now();

        String formattedDate;
        formattedDate = today.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        Date fechaInicial = dateFormat.parse(formattedDate);
        Date fechaFinal = dateFormat.parse(fecha.getValue().toString());

        long in = fechaInicial.getTime();
        long fin = fechaFinal.getTime();
        Long diff = (in - fin) / 1000;
        return diff.intValue() / (60 * 60 * 24 * 365);
    }
    public void BUSCAR(ActionEvent event) throws Exception {
        String user = userName.getText();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setTitle("Información");
        alert.setContentText(gestorAd.buscarAdministrador(user));
        alert.showAndWait();
        alert.setTitle("Info");
        alert.showAndWait();
    }

    public void IRSUBASTA(ActionEvent event) throws Exception {
        new ToScene().toScene("subasta.fxml", event);

    }

    public void LISTCC(ActionEvent event) throws Exception {
        List<String> lista = gestorColec.listarColeccionistas();
        ObservableList<String> observableList = FXCollections.observableList(lista);
        lvc.setItems(observableList);
    }

    public void ELIMINARCC(ActionEvent event) throws Exception {
        String userNameColec = userColecc.getText();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);

        alert.setHeaderText(null);
        alert.setTitle("Información");
        alert.setContentText(gestorColec.eliminarColeccionista(userColecc.getText()));
        alert.showAndWait();
        alert.setTitle("Info");
        alert.setContentText("ColeccionistaEliminado");
        alert.showAndWait();
    }

    public void BUSCARCC(ActionEvent event) throws Exception {
        String userNameColec = userColecc.getText();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setTitle("Información");
        alert.setContentText(gestorColec.buscarColeccionista(userNameColec));
        alert.showAndWait();
        userName.clear();

    }
    /*
        String userNameColec = labelBORRAR.getText();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);

        alert.setHeaderText(null);
        alert.setTitle("Información");
        alert.setContentText(gestorColec.eliminarColeccionista(labelBORRAR.getText()));
        alert.showAndWait();
     */
}