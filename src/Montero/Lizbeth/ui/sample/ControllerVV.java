package Montero.Lizbeth.ui.sample;

import Montero.Lizbeth.tl.ControllerItem;
import Montero.Lizbeth.tl.ControllerVendedor;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.time.LocalDate;
import java.util.List;

public class ControllerVV {

    @FXML
    private TextField id;

    @FXML
    private TextField nom;

    @FXML
    private TextField apell;

    @FXML
    private TextField correo;

    @FXML
    private TextField direc;

    @FXML
    private TextField puntuac;

    @FXML
    private TextField cod;

    @FXML
    private TextField imagen;

    @FXML
    private TextField estado;

    @FXML
    private TextField descr;

    @FXML
    private DatePicker fecha;

    @FXML
    private TextField antiguedad;

    @FXML
    private TextField codCategoria;
    @FXML
    private ListView<String> lvListView;
    @FXML
    private ListView<String> lvo;
    @FXML
    private Button RISOBJ;
    static ControllerVendedor gestorVen;

    static {
        try {
            gestorVen = new ControllerVendedor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    static ControllerItem gestorIT;
    static {
        try {
            gestorIT = new ControllerItem();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ingresarV(ActionEvent event) throws Exception {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        String identi = id.getText();
        String name = nom.getText();
        String apellido = apell.getText();
        String corr = correo.getText();
        String d = direc.getText();
        double punt = Double.parseDouble(puntuac.getText());
        String busqueda = gestorVen.buscarVendedor(identi);
        String vend = gestorVen.buscarVendedor(identi);
        //la validación del usuario

        if (!vend.equals("no existe")) {
            String[] v = busqueda.split(",");

            alert.setTitle("Información");
            alert.setContentText(gestorVen.registrarVendedores(identi, name, apellido, corr, d, punt));
            alert.showAndWait();
        } else {
            alert.setHeaderText(null);
            alert.setTitle("Info");
            alert.setContentText("Los datos que ingresó no coinciden ");
            alert.showAndWait();
        }

        id.clear();
        nom.clear();
        apell.clear();
        correo.clear();
        direc.clear();
        puntuac.clear();
    }

    public void registrarOBJETO(ActionEvent event) throws Exception {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        String codigo= cod.getText();
        LocalDate fc = LocalDate.parse(fecha.getValue().toString());
        String nombre= nom.getText();
        String descripcion=  descr.getText();
        String es=  estado.getText();
        String imag=  imagen .getText();
        String antigu=  antiguedad.getText();
        String cadCate=  codCategoria.getText();
        String busqueda = gestorVen.buscarItem(codigo);
        String vend = gestorVen.buscarItem(codigo);

        if (!vend.equals("no existe")) {
            String[] v = busqueda.split(",");
            alert.setTitle("Información");
            alert.setContentText(gestorIT.registrarItem( codigo,nombre,descripcion,es,imag,fc,antigu,cadCate));
            alert.showAndWait();
        } else {
            alert.setHeaderText(null);
            alert.setTitle("Info");
            alert.setContentText("Los datos que ingresó no coinciden ");
            alert.showAndWait();
        }
    }

    public void LISTARVENDEDOR(ActionEvent event) throws Exception {
        List<String> lista = gestorVen.listarVendedor();
        ObservableList<String> observableList = FXCollections.observableList(lista);
        lvListView.setItems(observableList);
    }

    public void LISTAROB(ActionEvent event) throws Exception {
            List<String> lista = gestorIT.listarObjeto();
        ObservableList<String> observableList = FXCollections.observableList(lista);
        lvo.setItems(observableList);

    }

    public void ELIMINAROBJETO(ActionEvent event) throws Exception {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        String codigo= cod.getText();

        alert.setHeaderText(null);
        alert.setTitle("Información");
        alert.setContentText(gestorIT.eliminarItem(codigo));
        alert.showAndWait();

    }

    public void ORDEN(ActionEvent event) throws Exception {
        new ToScene().toScene("subasta.fxml", event);
    }

}
