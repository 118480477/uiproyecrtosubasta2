package Montero.Lizbeth.ui.sample;

import Montero.Lizbeth.tl.ControllerColeccionista;
import Montero.Lizbeth.tl.ControllerItem;
import Montero.Lizbeth.tl.ControllerOferta;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

public class ControllerColecc implements Initializable {

    @FXML
    private Button volverColecc;

    @FXML
    private Button imprimir;

    @FXML
    private ListView<String> lvListView;

    @FXML
    private TextField identificacion;

    @FXML
    private TextField moderador;

    @FXML
    private TextField direccion;

    @FXML
    private TextField cali;

    @FXML
    private TextField estado;

    @FXML
    private TextField contra;

    @FXML
    private TextField correo;

    @FXML
    private TextField edad;

    @FXML
    private DatePicker fecha;

    @FXML
    private TextField avatar;

    @FXML
    private TextField nombre;

    @FXML
    private TextField tipo;

    @FXML
    private Button Rcolecc;

    @FXML
    private Button modifi;
    @FXML
    private TextField userName;
    @FXML
    private ImageView ivImagen;

    @FXML
    private TextField codoferta;

    @FXML
    private TextField precio;

    @FXML
    private TextField user;

    @FXML
    private TextField codiSubasta;
    @FXML
    private Button MISOBJETOS;
    @FXML
    private ListView<String> items;

    @FXML
    private AnchorPane idPaneColecc;
    private String path = null;
    private FileChooser fileChooser;
    private File file;
    private Stage stage;
    private final Desktop deskTop = Desktop.getDesktop();
    private Image image;


    static ControllerOferta gestorOferta;

    static {
        try {
            gestorOferta = new ControllerOferta();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static ControllerColeccionista gestorColec;

    static {
        try {
            gestorColec = new ControllerColeccionista();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    static ControllerItem gestorit;

    static {
        try {
            gestorit = new ControllerItem();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void PrincipalColecc(ActionEvent event) throws Exception {

        new ToScene().toScene("prueba.fxml", event);
    }

    @FXML
    public void listar(ActionEvent event) throws Exception {

        List<String> lista = gestorColec.listarColeccionistas();
        ObservableList<String> observableList = FXCollections.observableList(lista);
        lvListView.setItems(observableList);
    }

    @FXML
    public void Registrar(ActionEvent event) throws Exception {

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        String userNameColec = userName.getText();
        String nombreUsuario = nombre.getText();
        String correoUsuario = correo.getText();
        String identificacionCOL = identificacion.getText();
        String direccionCOL = direccion.getText();
        String estadoCOL = estado.getText();
        double clificacion = Double.parseDouble(cali.getText());
        int edadCole = calcularEdad(Integer.parseInt(edad.getText()));//calcularEdad
        LocalDate nacimiento = LocalDate.parse(fecha.getValue().toString());
        String contraseniaUsuario = contra.getText();
        String avatarC = avatar.getText();
        int modedarador = Integer.parseInt((moderador.getText()));
        String busqueda = gestorColec.buscarColeccionista(correoUsuario);
        String coleccionista = gestorColec.buscarColeccionista(correoUsuario);
        String tipoUsuario = tipo.getText();

        gestorColec.registrarColeccionista(identificacionCOL, tipoUsuario, nombreUsuario, avatarC, nacimiento, edadCole,
                userNameColec, correoUsuario, contraseniaUsuario, estadoCOL,
                clificacion, direccionCOL, modedarador);

        nombre.clear();
        userName.clear();
        contra.clear();
        tipo.clear();
        avatar.clear();
        moderador.clear();
        edad.clear();
        identificacion.clear();
        cali.clear();
        correo.clear();
        estado.clear();
        direccion.clear();


    }

    public void modificarC(ActionEvent event) throws Exception {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        String userNameColec = userName.getText();
        String nombreUsuario = nombre.getText();
        String correoUsuario = correo.getText();
        String identificacionCOL = identificacion.getText();
        String direccionCOL = direccion.getText();
        String estadoCOL = estado.getText();
        double clificacion = Double.parseDouble(cali.getText());
        int edadCole = Integer.parseInt(edad.getText());
        DatePicker nacimiento = fecha;
        String contraseniaUsuario = contra.getText();
        //TODO
        if (file.getAbsolutePath() != null) {
            String avatarC = file.getAbsolutePath();
        }

        int modedarador = Integer.parseInt((moderador.getText()));
        String busqueda = gestorColec.buscarColeccionista(correoUsuario);
        String coleccionista = gestorColec.buscarColeccionista(correoUsuario);
        String tipoUsuario = tipo.getText();

        alert.setContentText(gestorColec.modificarColeccionista(identificacion.getText(), tipo.getText(), nombre.getText(), avatar.getText(), LocalDate.parse(fecha.getValue().toString()), edad.getPrefColumnCount(), userName.getText(),
                correo.getText(), contra.getText(), estado.getText(), cali.getMaxHeight(), direccion.getText(), moderador.getPrefColumnCount()));
        alert.showAndWait();
        /*alert.setTitle("info");
        alert.setContentText("Coleccionista Modificado correctamente");
        alert.showAndWait();*/
        // borrar
        nombre.clear();
        userName.clear();
        contra.clear();
        tipo.clear();
        avatar.clear();
        moderador.clear();
        edad.clear();
        identificacion.clear();
        cali.clear();
        correo.clear();
        estado.clear();
        direccion.clear();
        fecha.setValue(null);


    }

    public void irCategoria(ActionEvent event) throws Exception {

        new ToScene().toScene("categoria.fxml", event);
    }

    public void ELIMINAR(ActionEvent event) throws Exception {
        String userNameColec = userName.getText();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);

        alert.setHeaderText(null);
        alert.setTitle("Información");
        alert.setContentText(gestorColec.eliminarColeccionista(userName.getText()));
        alert.showAndWait();
        alert.setTitle("Info");
        alert.setContentText("ColeccionistaEliminado");
        alert.showAndWait();

    }

    public void BUSCAR(ActionEvent event) throws Exception {
        String userNameColec = userName.getText();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setTitle("Información");
        alert.setContentText(gestorColec.buscarColeccionista(userNameColec));
        alert.showAndWait();
        userName.clear();
    }

    public int calcularEdad(int i) throws ParseException {
        int edad = 0;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        LocalDate today = LocalDate.now();

        String formattedDate;
        formattedDate = today.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        Date fechaInicial = dateFormat.parse(formattedDate);
        Date fechaFinal = dateFormat.parse(fecha.getValue().toString());

        long in = fechaInicial.getTime();
        long fin = fechaFinal.getTime();
        Long diff = (in - fin) / 1000;
        return diff.intValue() / (60 * 60 * 24 * 365);
    }


    public void avatarELEGIR(ActionEvent event) throws IOException {

        stage = (Stage) idPaneColecc.getScene().getWindow();
        file = fileChooser.showOpenDialog(stage);

        if (file != null) {
            System.out.println("" + file.getAbsolutePath());
            try {
                image = new Image(file.getAbsoluteFile().toURL().toString(), ivImagen.getFitWidth(), ivImagen.getFitWidth(), true, true);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IncompatibleClassChangeError e) {
                e.printStackTrace();
            }
            ivImagen.setImage(image);
            ivImagen.setPreserveRatio(true);
        }
    }

    public void REGISTRARI(ActionEvent event) throws Exception {
        new ToScene().toScene("iObejto.fxml", event);
    }


    public void VEROBJE(ActionEvent event) throws Exception {
        String userNameColec = userName.getText();
        List<String> lista = gestorit.listarObjeto();
        ObservableList<String> observableList = FXCollections.observableList(lista);
        items.setItems(observableList);

    }

    public void OFERTAR(ActionEvent event) throws Exception {

        new ToScene().toScene("ofer.fxml", event);


    }

    public void AOBJETOS(ActionEvent event) {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("images", "*.png", "*.jpg", "*.gif"),
                new FileChooser.ExtensionFilter("All files", "* *"),
                new FileChooser.ExtensionFilter("Text File", "*.txt")
        );
    }
}