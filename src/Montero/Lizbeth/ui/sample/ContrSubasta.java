package Montero.Lizbeth.ui.sample;

import Montero.Lizbeth.tl.ControllerItem;
import Montero.Lizbeth.tl.ControllerOferta;
import Montero.Lizbeth.tl.ControllerSubasta;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;


import java.time.LocalDate;
import java.util.List;

public class ContrSubasta {
    static ControllerSubasta gestorSubasta;

    static {
        try {
            gestorSubasta = new ControllerSubasta();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private TextField codS;

    @FXML
    private TextField codCompra;


    @FXML
    private TextField idUusario;


    @FXML
    private TextField HVENCIMIENTO;


    @FXML
    private TextField precio;


    @FXML
    private TextField hINICIO;


    @FXML
    private DatePicker Fcreacion;

    @FXML
    private DatePicker fInicio;


    @FXML
    private DatePicker fVencimiento;

    @FXML
    private RadioButton no;

    @FXML
    private RadioButton activo;
    @FXML
    private Button regSubasta;

    @FXML
    private Button listarS;
    @FXML
    private CheckBox EACTIVA;

    @FXML
    private CheckBox inactiva;

    @FXML
    private ListView<String> lisView;

    @FXML
    private Button eliminar;

    @FXML
    private Button BUSCAR;

    @FXML
    private TextField subCod;

    @FXML
    private TextField itemcodigo;

    @FXML
    private Button HACEROferta;

    @FXML
    private Button orden;

    @FXML
    private TextField codoferta;

    @FXML
    private DatePicker fechasOferta;
    @FXML
    private TextField max;

    @FXML
    private Button info;
    @FXML
    private TextField ofertasCant;

    @FXML
    private ListView<String> ll;

    @FXML
    private ListView<String> ll1;


    @FXML
    private TextField preciOFE;

    @FXML
    private TextField userColecc;

    @FXML
    private CheckBox cerrarSu;

    @FXML
    private AnchorPane anchor11;


    static ControllerOferta gestorOferta;

    static {
        try {
            gestorOferta = new ControllerOferta();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    static ControllerItem gestorI;

    static {
        try {
            gestorI = new ControllerItem();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void AGREAGRob(ActionEvent event) throws Exception {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        String sub = subCod.getText();
        String itemco = itemcodigo.getText();
        alert.setContentText(gestorSubasta.asociarObjetosSubasta(sub, itemco));

    }

    @FXML
    void BUSCARs(ActionEvent event) throws Exception {
        String codigoSubasta = codS.getText();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setTitle("Información");
        alert.setContentText(gestorSubasta.buscarSubasta(codigoSubasta));
        alert.showAndWait();
        codS.clear();
    }

    @FXML
    void ELIMINARs(ActionEvent event) throws Exception {
        String codigoSubasta = codS.getText();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        gestorSubasta.eliminarSubasta(codigoSubasta);
        alert.setHeaderText(null);
        alert.setTitle("Información");
        alert.showAndWait();
        alert.setTitle("Info");
        alert.setContentText("Subasta Eliminada");
        alert.showAndWait();
    }

    @FXML
    void listarSUBASTA(ActionEvent event) throws Exception {
        List<String> lista = gestorSubasta.listarSubasta();
        ObservableList<String> observableList = FXCollections.observableList(lista);
        lisView.setItems(observableList);
    }

    @FXML
    void registrar(ActionEvent event) throws Exception {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        String CODSUB = codS.getText();
        LocalDate FECHCREA = LocalDate.parse(Fcreacion.getValue().toString());
        int HORAINI = (Integer.parseInt(hINICIO.getText()));
        String COGCOMPRA = codCompra.getText();
        LocalDate FEINI = LocalDate.parse(fInicio.getValue().toString());
        String USER = idUusario.getText();
        double PB = Double.parseDouble(precio.getText());
        LocalDate FECHAVEN = LocalDate.parse(fVencimiento.getValue().toString());
        int HORAV = (Integer.parseInt(HVENCIMIENTO.getText()));
        String busqueda = gestorSubasta.buscarSubasta(CODSUB);
        String subasta = gestorSubasta.buscarSubasta(CODSUB);
        String estado = "";
        //la validacion del check de la acreditacion y la activacion
        if (EACTIVA.isSelected()) {
            estado = "activa";

        } else if ( inactiva.isSelected()){
            estado = "inactiva";

        }else{
            cerrarSu.isSelected();
            estado="Cerrada";
        }

        if (CODSUB.equals(gestorSubasta.buscarSubasta(CODSUB))) {

            alert.setTitle("info");
            alert.setContentText("BIEN ");
            alert.showAndWait();
        } else {
            alert.setHeaderText(null);
            alert.setTitle("info");
            String E=estado;
            alert.setContentText(gestorSubasta.registrarSubasta(CODSUB, FECHCREA, FEINI, HORAINI, FECHAVEN, HORAV,estado , PB, COGCOMPRA, USER));

            alert.showAndWait();

         /*  codS.clear();*
            codCompra.clear();
            hINICIO.clear();
            precio.clear();*/
            // new ToScene().toScene("Comienzo.fxml", event);
        }
    }

    public void VOLVER(ActionEvent event) throws Exception {
        new ToScene().toScene("prueba.fxml", event);
    }

    public void OFERTAR(ActionEvent event) throws Exception {

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        String CODSUB = codS.getText();
        LocalDate FECHCREA = LocalDate.now();
       // int HORAINI = (Integer.parseInt(hINICIO.getText()));

        String USER = userColecc.getText();
        double PB = Double.parseDouble(preciOFE.getText());
        //LocalDate FECHAVEN = LocalDate.parse(fVencimiento.getValue().toString());
        //int HORAV = (Integer.parseInt(HVENCIMIENTO.getText()));
        String codigo= codoferta.getText();
        LocalDate FEcha = LocalDate.now();
        String busqueda = gestorOferta.buscarOferta(codigo);

        //la validacion del check de la acreditacion y la activacion
        if (codigo.equals(gestorOferta.buscarOferta(codigo))) {
            alert.setTitle("info");
            alert.setContentText("esta oferta ya existe");
            alert.showAndWait();
        } else {
            alert.setHeaderText(null);
            alert.setTitle("info");
            alert.setContentText(gestorSubasta.asociarOfertasSubasta(codigo,PB,FEcha,USER,CODSUB));
            alert.showAndWait();
        }
    }


    public void Infos(ActionEvent event) throws Exception {
        String CODSUB = codS.getText();
        ofertasCant.setText(gestorSubasta.listarCantidadOfertas(CODSUB)+"");
        max.setText(gestorSubasta.listarMayorOferta(CODSUB)+"");

    }

    public void LISTARI(ActionEvent event) throws Exception {
        String CODSUB = codS.getText();
        List<String> lista = gestorSubasta.listarObjetosSubasta(CODSUB);
        ObservableList<String> observableList = FXCollections.observableList(lista);
        ll.setItems(observableList);
    }

    public void ListarOfer(ActionEvent event) throws Exception {
        String CODSUB = codS.getText();

        List<String> lista = gestorOferta.listarOferta();
        ObservableList<String> observableList = FXCollections.observableList(lista);
        ll1.setItems(observableList);

    }

    public void CERRARSUBASTA(ActionEvent event) {
    }
//TODO
    public void IrOrdenCompra(ActionEvent event) throws Exception {

        new ToScene().toScene("orden.fxml", event);


    }

    public void ORDEN(ActionEvent event) throws Exception {

        new ToScene().toScene("o.fxml", event);

    }



    public void LISTAROBJETO(ActionEvent event) throws Exception {

        List<String> lista = gestorI.listarObjeto();
        ObservableList<String> observableList = FXCollections.observableList(lista);
        ll.setItems(observableList);
    }

    public void ordenn(ActionEvent event) throws Exception {
        new ToScene().toScene("o.fxml", event);
    }
}









