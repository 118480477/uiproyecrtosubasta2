package Montero.Lizbeth.ui.sample;

import Montero.Lizbeth.tl.ControllerOferta;
import Montero.Lizbeth.tl.ControllerOrden;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

import java.awt.*;
import java.time.LocalDate;

public class O {
    @FXML
    private TextField codigo;

    @FXML
    private TextField precio;

    @FXML
    private TextField hora;

    @FXML
    private TextField user;

    @FXML
    private TextField codSubasta;

    @FXML
    private DatePicker fecha;

    @FXML
    private Button AG;

    @FXML
    private Button E;

    @FXML
    private Button B;
    static ControllerOrden gestorO;

    static {
        try {
            gestorO = new ControllerOrden();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @FXML
    void AGREGAR(ActionEvent event) throws Exception {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        String codOrden = codigo.getText();
        String codS = codSubasta.getText();
        String us = user.getText();
        String iden="1";
        double precs = Double.parseDouble(precio.getText());
        int ho = (Integer.parseInt(hora.getText()));
        LocalDate nn = LocalDate.parse(fecha.getValue().toString());
        gestorO.registrarOrdenCompra(codOrden,nn,ho,precs,iden,codS,us);
        alert.setTitle("Información");
        alert.setContentText("Registrado correctamente");
        alert.showAndWait();

    }

    @FXML
    void BUSCAR(ActionEvent event) {

    }

    @FXML
    void ELIMI(ActionEvent event) {

    }

    public void Buscar(ActionEvent event) throws Exception {

        String co = codigo.getText();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setTitle("Información");
        alert.setContentText(gestorO.buscarOrdenCompra(co));
        alert.showAndWait();

    }
}
